//
//  ViewController.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/1/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    //MARK: - UIViewController overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeViewDetails()
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier == "PropertyUnitsSegue") {
//            if let unitsViewController = segue.destination as? UnitsTableViewController {
//                // future implementaion
//            }
//        }
//    }
    
    //MARK: - IBActions
    
    @IBAction func managePropertyUnitsButtonTouchUp(_ sender: UIButton) {
        self.performSegue(withIdentifier: "PropertyUnitsSegue", sender: sender)
    }
    
    
    //MARK: - Private API
    
    private func initializeViewDetails() {
        self.navigationItem.title = "Tenantor! Home "
    }

}

