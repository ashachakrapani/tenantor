//
//  UnitTableViewCell.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/1/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import UIKit

class UnitsTableViewCell: UITableViewCell {

    //MARK: - IBOutlet
    
    @IBOutlet weak var unitNumber: UILabel!
    @IBOutlet weak var rent: UILabel!
    
    //MARK: - UITableviewCell Overrides
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public API
    
    func resetCell() {
        self.unitNumber.text = ""
        self.rent.text = ""
    }

}

