//
//  UnitsViewController.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/1/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import UIKit

class UnitsTableViewController: UIViewController {
    
    //MARK: - UIViewController overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 75
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.setUpNavigationBar()
        
        if let _unitsListViewModel = self.fetchUnits() {
            self.unitsListViewModel = _unitsListViewModel
            self.tableView.reloadData()
        }
    }
    
    
    //MARK: - Private API
    
    private lazy var coreDataController = CoreDataController()
    private var unitsListViewModel: [UnitViewModel]?
    
    private func setUpNavigationBar() {
        let addTaskNavBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addUnitButtonTouchUp(sender:)))
        self.navigationItem.rightBarButtonItem = addTaskNavBarButton
        
        self.navigationItem.title = NSLocalizedString("Property Units", comment: "Title for property units list table")
        self.navigationItem.leftBarButtonItem?.title = NSLocalizedString("Back", comment: "Title for property units list table")
    }
    
    @objc private func addUnitButtonTouchUp(sender: UIBarButtonItem) {
        let addUnitViewController = UIStoryboard(name: "AddUnit", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddUnitViewController") as! AddUnitViewController
        addUnitViewController.delegate = self
        self.navigationController?.pushViewController(addUnitViewController, animated: false)
    }
    
    private func fetchUnits() -> [UnitViewModel]? {
        if let unitsMOC: [UnitMOC] = self.coreDataController.fetchUnits() {
            let unitsViewModel: [UnitViewModel] = unitsMOC.map { (unitMoc) -> UnitViewModel in
            //Unit view model for data v & v2
//                return UnitViewModel(unitId: unitMoc.unitId,
//                                     landlordName: unitMoc.landlord?.name,
//                                     tenantName: unitMoc.tenant?.name,
//                                     notes: unitMoc.notes)
                //Unit view model instance for datamodel v3
                return UnitViewModel(unitId: unitMoc.unitId,
                                     rent: Int(unitMoc.rent),
                                     landlordName: unitMoc.landlord?.name,
                                     tenantName: unitMoc.tenant?.name,
                                     notes: unitMoc.notes)
                
                //Unit view model instance for datamodel v4
//                return UnitViewModel(unitId: unitMoc.unitId,
//                                     rent: Int(unitMoc.lease?.rent ?? 0),
//                                     landlordName: unitMoc.landlord?.name,
//                                     tenantName: unitMoc.tenant?.name,
//                                     notes: unitMoc.notes)

            }
            return unitsViewModel
        }
        return nil
    }
    
    private func refreshTableData(with unit: UnitViewModel) {
        
    }
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
}

//MARK: - UITableViewDelegate and UITableViewDataSource
extension UnitsTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.unitsListViewModel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "UnitTableViewCell", for: indexPath) as! UnitsTableViewCell
        cell.unitNumber.text = self.unitsListViewModel?[indexPath.row].unitId
        if let _rent = self.unitsListViewModel?[indexPath.row].rent {
            cell.rent.text = String(_rent)
        }

        cell.isUserInteractionEnabled = false
        return cell
    }
    
}

extension UnitsTableViewController: NewUnitDelegate {
    func added(newUnit unitViewModel: UnitViewModel) {
        self.unitsListViewModel?.append(unitViewModel)
        self.tableView.reloadData()
    }
    
}
