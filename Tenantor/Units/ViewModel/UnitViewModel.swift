//
//  UnitViewModel.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation

struct UnitViewModel {
    
    ///Gets or sets the unit id
    var unitId: String?
    
    //This is the lease information for a unit, which will eventually be moved to a difference data model object
    ///Gets or sets the rent for the unit
    var rent: Int?
    
    ///Gets or sets the landlord's name
    var landlordName: String?
    
    ///Gets or sets the tenant's name
    var tenantName: String?
    
    ///Gets or sets notes associated with the unit
    var notes: String?
    
//    static func getNewInstance() -> UnitViewModel {
//        return UnitViewModel(unitId: nil, landlordName: nil, tenantName: nil, notes: nil)
//    }
    
        static func getNewInstance() -> UnitViewModel {
            return UnitViewModel(unitId: nil, rent: nil, landlordName: nil, tenantName: nil, notes: nil)
        }
}
