//
//  V3ToV4MigrationPolicy.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/3/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import CoreData

class V3ToV4MigrationPolicy: NSEntityMigrationPolicy {
    
    override func createDestinationInstances(forSource sInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        try super.createDestinationInstances(forSource: sInstance, in: mapping, manager: manager)
        
        let rent = sInstance.value(forKey: "rent") as! Int32
        let unitId = sInstance.value(forKey: "unitId") as! String
        if let leaseEntity = NSEntityDescription.entity(forEntityName: "Lease", in: manager.destinationContext) {
            let leaseInstance = NSManagedObject(entity: leaseEntity, insertInto: manager.destinationContext)
            leaseInstance.setValue(rent, forKey: "rent")
            leaseInstance.setValue(unitId, forKey: "unitId")
        }
    }
    
    override func  createRelationships(forDestination dInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        try super.createRelationships(forDestination: dInstance, in: mapping, manager: manager)
        

        if let entityName = dInstance.entity.name, entityName == "Unit", let unitId = dInstance.value(forKey: "unitId") as? String {
            //create relationship between Unit and Lease
            //find the lease instance associated with the unit id derived from this instance of unit and set the relationship. This look up can be done between source and destination instances as well. The most important is that MOC subclasses cannot be used, although they will compile, it will fail at runtime.
            if let dLeaseInstance = self.findLeaseDestinationInstance(forUnitId: unitId, nsMigrationManager: manager) {
                dInstance.setValue(dLeaseInstance, forKey: "lease")
            }
        }
    }
    
    //MARK: Private API
    private func findLeaseDestinationInstance(forUnitId unitId: String, nsMigrationManager manager: NSMigrationManager) -> NSManagedObject? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Lease")
        fetchRequest.predicate = NSPredicate(format: "unitId == \(unitId)")
        fetchRequest.resultType = .managedObjectResultType
        do {
            let leaseResults = try manager.destinationContext.fetch(fetchRequest)
            for leaseEntity in leaseResults {
                return leaseEntity as? NSManagedObject
            }
        } catch {
            print("Error fetching lease instance in the destination context \(error)")
        }
        return nil
    }
}

