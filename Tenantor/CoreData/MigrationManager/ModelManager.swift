//
//  ModelManager.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 11/5/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

public class ModelManager {
    
    //Determines the next higher version to the version passed in as the argument
    func next(version: StoreModelVersion?) -> StoreModelVersion? {
        //TODO: what if the version passed in is the destination version
        if let versionIndex = StoreModelVersion.allCases.firstIndex(where: { $0 == version }) {
            return StoreModelVersion.allCases[StoreModelVersion.allCases.index(after: versionIndex)]
        }
        // this should never be nil
        return nil
    }
    
    //TODO: what if there is an issue computing any of these parameters?
    func migrationModel(forSourceVersion sourceVersion: StoreModelVersion) -> MigrationModel {
        var mappingModel: NSMappingModel?
        
        let sourceStoreUrl = CoreDataStack.sharedInstance.persistentStoreURL
        let destinationStoreUrl = URL(fileURLWithPath:NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID().uuidString)

        guard let sourceStoreModel = self.getModel(forVersion: sourceVersion) else { fatalError("error obtaining source model for source version \(sourceVersion)") }
       
        guard let destinationStoreModel = self.getDestinationModel(sourceVersion: sourceVersion) else { fatalError("error obtaining destination model for source version \(sourceVersion)") }
        
        //A heavyweight migration always has a mapping model defined in the StoreModelVersion
        if let mappingModelName = StoreModelVersion.mappingModelName(forSourceVersion: sourceVersion) {
            let mappingModelUrl = self.getMappingModelUrl(forMappingModelName: mappingModelName)
            mappingModel = NSMappingModel.init(contentsOf: mappingModelUrl)
        } else {
            //mapping model can always be inferred for a light weight migration
            mappingModel = self.inferredMigrationModel(forSource: sourceStoreModel, destinationModel: destinationStoreModel)
        }
        
        guard let _mappingModel = mappingModel else { fatalError("error obtaining source model for source version \(sourceVersion)") }
        
        let migrationModel = MigrationModel.init(sourceStoreUrl: sourceStoreUrl, sourceStoreModel: sourceStoreModel, destinationStoreUrl: destinationStoreUrl, destinationStoreModel: destinationStoreModel, mappingModel: _mappingModel)
        
        return migrationModel
    }
    
    
    func findSourceModelVersion() -> StoreModelVersion? {
        if let storeMetadata: [String: Any] = self.getStoreMetadata() {
            for version in StoreModelVersion.allCases {
                if let modelForVersion = self.getModel(forVersion: version) {
                    if (modelForVersion.isConfiguration(withName: "Tenantor", compatibleWithStoreMetadata: storeMetadata)) {
                        return version
                    }
                }
            }
        }
        return nil
    }
    
    func getStoreMetadata() -> [String: Any]? {
        if let storeUrl = CoreDataStack.sharedInstance.persistentContainer.persistentStoreDescriptions.first?.url {
            do {
                let metadata = try NSPersistentStoreCoordinator.metadataForPersistentStore(ofType: NSSQLiteStoreType, at: storeUrl)
                return metadata
            } catch {
                print("Error getting source store metadata \(error) - this is only expected to be seen the very first time the app is run")
            }
        }
        return nil
    }
    
    func getModel(forVersion version: StoreModelVersion) -> NSManagedObjectModel? {
        if let momUrl = self.getModelUrl(forVersion: version) {
            return NSManagedObjectModel.init(contentsOf: momUrl)
        }
        return nil
    }
    
    func getDestinationModel(sourceVersion: StoreModelVersion) -> NSManagedObjectModel? {
         if let nextVersion = self.next(version: sourceVersion) {
            return self.getModel(forVersion: nextVersion)
        }
        return nil
    }
    
    func inferredMigrationModel(forSource sourceModel: NSManagedObjectModel, destinationModel: NSManagedObjectModel) -> NSMappingModel? {
        var inferredMappingModel: NSMappingModel? = nil
        do {
            inferredMappingModel = try NSMappingModel.inferredMappingModel(forSourceModel: sourceModel, destinationModel: destinationModel)
        } catch {
            print("Error attempting to infer mapping model between source \(sourceModel) and destination \(destinationModel)")
        }
        return inferredMappingModel
    }
    
    func getMappingModelUrl(forMappingModelName mappingModel: String) -> URL? {
        let momUrl = Bundle.main.url(forResource: mappingModel, withExtension: "cdm")
        return momUrl
    }
    
    //TODO: Datamodel names are hardcoded.
    func getModelUrl(forVersion version: StoreModelVersion) -> URL? {
        if let _ = Bundle.main.url(forResource: "Tenantor", withExtension: "momd") {
            let momUrl = Bundle.main.url(forResource: version.name, withExtension: "mom", subdirectory: "Tenantor.momd")
            return momUrl
        } else {
            //Assuming the app cannot function without coredata for which the store model directory is mandatory
            fatalError("Unable to locate store model directory.")
        }
    }
    
    func getVersion(forModel model: NSManagedObjectModel) -> StoreModelVersion? {
        let currentSourceModel = model
        for version in StoreModelVersion.allCases {
            if let modelForVersion = self.getModel(forVersion: version) {
                if (currentSourceModel == modelForVersion) {
                    return version
                }
            }
        }
        return nil
    }
}
