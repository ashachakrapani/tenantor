//
//  MigrationModel.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 11/2/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

struct MigrationModel {
    //ideally none of these must be optional
    var sourceStoreUrl: URL // url of the location where the store the current store exists before migration
    var sourceStoreModel: NSManagedObjectModel // model of the store before migration
    var destinationStoreUrl: URL // url of the location where the store the current store exists before migration
    var destinationStoreModel: NSManagedObjectModel // this is a temporary location where the migrated store is created during the migration process
    var mappingModel: NSMappingModel //mapping model that allows a migration from the source to the destination. It is either inferred (for lightweight) or manually constructed(heavy weight)
}
