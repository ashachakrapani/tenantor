//
//  StoreModelVersionKeys.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 10/30/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
//This enum serves as a look up for all version of the model in the app and the string value represents the file name of the corresponding .xcdatamodel(compiled to .mom) file within the xcdatamodeld directory
public enum StoreModelVersion: Int {
    case version1 = 1
    case version2 = 2
    case version3 = 3
    case version4 = 4
//    case version5 = 5
//    case version6 = 6

    
    var name: String {
        switch self {
        case .version1:
            return "Tenantor"
        case .version2:
            return "Tenantor v2"
        case .version3:
            return "Tenantor v3"
        case .version4:
            return "Tenantor v4"
//        case .version5:
//            return "Tenantor v5"
//        case .version6:
//            return "Tenantor v6"
        }
    }
}

extension StoreModelVersion: CaseIterable {}

extension StoreModelVersion {
    
    //Not having a mapping model defined here implies that a light weight migration is possible from the corresponding source to destination version
    static func mappingModelName(forSourceVersion version: StoreModelVersion) -> String? {
        switch version {
        case .version3:
            return "V3toV4MappingModel"
        default:
            return nil
        }
    }

}

