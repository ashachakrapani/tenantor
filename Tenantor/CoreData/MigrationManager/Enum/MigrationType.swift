//
//  MigrationType.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 11/2/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public enum MigrationType: Int {
    case unknown = 0
    case lightweight
    case heavyweight
}
