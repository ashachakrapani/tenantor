//
//  MigrationManager.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 10/26/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import CoreData

class MigrationManager: NSMigrationManager {
    
    //MARK: - Singleton Implementation
    
    private override init() {
        super.init()
    }
    
    //MARK: - Public Api
    static var sharedInstance = MigrationManager()
    
    func setUpStore(completion: @escaping () -> Void) {
        //1. get version of the model marked as current i.e. destination model version
        //2. get the model version the store is mapped to i.e. source model version
        //3. get the MigrationModel required to go from model in source to destination
        
        //checks if an existing store needs to be migrated, if not just attempts to load the store as defined in the CoreDataStack
        if (self.isMigrationRequired()) {
            let migrationModels = self.getMigrationLevels()
            for migrationModel in migrationModels {
                //perform migration
                CoreDataStack.sharedInstance.disableAutomaticLightweightMigration()
                self.migrate(migrationModel: migrationModel)
            }
        }
        completion()
    }
    
    //MARK: - Private Api
    private lazy var modelManager = ModelManager()
    
    //Returns whether the model marked as 'current' matches the current store or not
    private func isMigrationRequired() -> Bool {
        if let storeMetadata: [String: Any] = self.modelManager.getStoreMetadata() {
            return !(CoreDataStack.sharedInstance.persistentContainer.managedObjectModel.isConfiguration(withName: "Tenantor", compatibleWithStoreMetadata: storeMetadata))
        }
        // if the metadata is nil, the assumption either that there is no migration required possibly because the store is being set up the first time
        return false
    }
    
    //Performs the store migration based on the migration model provided
    private func migrate(migrationModel: MigrationModel) {
        print("----------- migrate ------------")
        print(migrationModel.destinationStoreUrl)
        print(migrationModel.sourceStoreUrl)
        
        do {
            let manager = NSMigrationManager(sourceModel: migrationModel.sourceStoreModel, destinationModel: migrationModel.destinationStoreModel)
            
            try manager.migrateStore(from: migrationModel.sourceStoreUrl, sourceType: NSSQLiteStoreType, options: nil, with: migrationModel.mappingModel, toDestinationURL: migrationModel.destinationStoreUrl, destinationType: NSSQLiteStoreType , destinationOptions: nil)
            
            try CoreDataStack.sharedInstance.persistentContainer.persistentStoreCoordinator.replacePersistentStore(at: migrationModel.sourceStoreUrl, destinationOptions: nil, withPersistentStoreFrom: migrationModel.destinationStoreUrl, sourceOptions: nil, ofType: NSSQLiteStoreType)
        } catch {
            print("error migrating store from source version  to destination : \(error)")
        }
    }
    
    //determines the number of levels required to go from existing model to the 'current' model
    private func getMigrationLevels() -> [MigrationModel] {
        
        let targetModelVersion = self.modelManager.getVersion(forModel: CoreDataStack.sharedInstance.persistentContainer.managedObjectModel)
        let sourceModelVersion = self.modelManager.findSourceModelVersion()
        
        var migrationLevels: [MigrationModel] = []
        
        if let _ = sourceModelVersion, let _ = targetModelVersion {
            print("currentStoreVersion \(sourceModelVersion!) finalDestinationStoreVersion \(String(describing: targetModelVersion))")
            
            if sourceModelVersion == targetModelVersion {
                return migrationLevels
            }
            
            let startingVersionIndex = StoreModelVersion.allCases.firstIndex(where: { $0 == sourceModelVersion}) ?? 0
            let endingVersionIndex = StoreModelVersion.allCases.firstIndex(where: { $0 == targetModelVersion}) ?? 0
            
            let olderVersionsToIgnore = StoreModelVersion.allCases.distance(from: StoreModelVersion.allCases.startIndex, to: startingVersionIndex)

            let newerVersionToIgnore = StoreModelVersion.allCases.distance(from: endingVersionIndex, to: StoreModelVersion.allCases.endIndex) 
            
            let modelVersionSubset = (StoreModelVersion.allCases.dropFirst(olderVersionsToIgnore)).dropLast(newerVersionToIgnore)
            
            print(modelVersionSubset)
            
            for version in (StoreModelVersion.allCases.dropFirst(olderVersionsToIgnore)).dropLast(newerVersionToIgnore) {
                migrationLevels.append(self.modelManager.migrationModel(forSourceVersion: version))
            }
        } else {
            print("unable to determine current and final store versions")
        }

        return migrationLevels
    }
}
