//
//  LandlordMOC.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import CoreData

@objc(LandlordMOC)
public class LandlordMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, fromDataContract dataContract: Landlord) -> LandlordMOC? {
        var objectMOC: LandlordMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Landlord", in: context) {
            objectMOC = LandlordMOC(entity: entity, insertInto: context)
            objectMOC?.name = dataContract.name
        }
        return objectMOC
    }
    
}
