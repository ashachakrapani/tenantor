//
//  UnitMOC.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//


import CoreData

@objc(UnitMOC)
public class UnitMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, fromDataContract dataContract: Unit) -> UnitMOC? {
        var objectMOC: UnitMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Unit", in: context) {
            objectMOC = UnitMOC(entity: entity, insertInto: context)
            objectMOC?.unitId = dataContract.unitId
            
            if let _landlord = dataContract.landlord {
                objectMOC?.landlord = LandlordMOC.insertInto(context: context, fromDataContract: _landlord)
            }
            
            if let _tenant = dataContract.tenant {
                objectMOC?.tenant = TenantMOC.insertInto(context: context, fromDataContract: _tenant)
            }
            //Unit moc addition for datamodel v3
            if let _rent = dataContract.rent {
                objectMOC?.rent = Int16(_rent)
            }
            //Unit moc addition for datamodel v4
//            if let _lease = dataContract.lease {
//                objectMOC?.lease = LeaseMOC.insertInto(context: context, fromDataContract: _lease)
//            }
        }
        return objectMOC
    }
    
}
