//
//  TenantMOC.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import CoreData

@objc(TenantMOC)
public class TenantMOC: NSManagedObject {
    
    static func insertInto(context: NSManagedObjectContext, fromDataContract dataContract: Tenant) -> TenantMOC? {
        var objectMOC: TenantMOC? = nil
        if let entity = NSEntityDescription.entity(forEntityName: "Tenant", in: context) {
            objectMOC = TenantMOC(entity: entity, insertInto: context)
            objectMOC?.name = dataContract.name
        }
        return objectMOC
    }
    
}
