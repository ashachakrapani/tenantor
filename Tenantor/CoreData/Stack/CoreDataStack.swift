//
//  CoreDataStack.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack: NSObject {
    
    //MARK: - Singleton Implementation
    
    private override init() {
        super.init()
    }
    
    static var sharedInstance = CoreDataStack()
    
    private var storeName = "Tenantor.sqlite"
    
    //MARK: - Public API
    
    var persistentStoreURL: URL {
        return AppFilePathKeys.appDocumentPath.appendingPathComponent(self.storeName)
    }
    
    var mainContext: NSManagedObjectContext {
        get {
            return self.persistentContainer.viewContext
        }
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Tenantor")
        print("Core data path : \(self.persistentStoreURL)" )
        let storeDescription = NSPersistentStoreDescription(url: self.persistentStoreURL)
        container.persistentStoreDescriptions = [storeDescription]
        return container
    }()
    
    func enableAutomaticLightweightMigration() {
        self.persistentContainer.persistentStoreDescriptions.first?.shouldInferMappingModelAutomatically = true
        self.persistentContainer.persistentStoreDescriptions.first?.shouldMigrateStoreAutomatically = true
    }
    
    func disableAutomaticLightweightMigration() {
        self.persistentContainer.persistentStoreDescriptions.first?.shouldInferMappingModelAutomatically = false
        self.persistentContainer.persistentStoreDescriptions.first?.shouldMigrateStoreAutomatically = false
    }
    
    func loadPersistentStore() -> Void {
        self.persistentContainer.loadPersistentStores(completionHandler: { (persistentStoreDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    
    func saveMainContext() {
        let context = self.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                //Fatal error here because store drives the app.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
