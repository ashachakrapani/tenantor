//
//  CoreDataController.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    
    func createNewUnit(unit: Unit) {
        let moc = CoreDataStack.sharedInstance.mainContext
        let _ = UnitMOC.insertInto(context: moc, fromDataContract: unit)
        do{
            try moc.save()
        } catch {
            print("Error inserting new unit into CoreData: \(error.localizedDescription)")
        }
    }
    
    func fetchUnits() -> [UnitMOC]? {
        let moc = CoreDataStack.sharedInstance.mainContext
        let fetchRequest: NSFetchRequest<UnitMOC> = UnitMOC.fetchRequest()
        
        var units: [UnitMOC]? = nil
        do {
            units = try moc.fetch(fetchRequest)
        } catch {
            print("Error fetching units from CoreData: \(error.localizedDescription)")
        }
        return units
    }
}
