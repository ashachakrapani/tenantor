//
//  AddUnitViewController.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/1/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import UIKit

protocol NewUnitDelegate: class {
    func added(newUnit unitViewModel: UnitViewModel)
}


enum TextFields: Int {
    case unitNumber = 1
    case landlordName = 2
    case tenantName = 3
    case rent = 4
    case notes = 5
}


class AddUnitViewController: UIViewController {
    
    //MARK: - UIViewController overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpNavigationBar()
        self.setUpTextFields()

    }
    
    //MARK: - IBOutlets
    
    @IBOutlet var unitInformationTextFields: [UITextField]!
    @IBOutlet weak var notes: UITextView!
    
    //MARK: - Private API
    
    var unitViewModel: UnitViewModel = UnitViewModel.getNewInstance()
    private lazy var coreDataController = CoreDataController()
    weak var delegate: NewUnitDelegate?
    
    
    private func setUpNavigationBar() {
        self.navigationItem.title = "Set up new unit"
        let addUnitNavBarButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveUnitButtonTouchUp(sender:)))
        self.navigationItem.rightBarButtonItem = addUnitNavBarButton
    }
    
    private func setUpTextFields() {
        self.notes.delegate = self
        self.unitInformationTextFields.forEach {
            $0.delegate = self
            $0.layer.borderWidth = 1
            $0.layer.borderColor = UIColor.lightGray.cgColor
            $0.layer.cornerRadius = 0
        }
        self.notes.layer.borderWidth = 1
        self.notes.layer.borderColor = UIColor.lightGray.cgColor
        self.notes.layer.cornerRadius = 0
    }
    
    @objc private func saveUnitButtonTouchUp(sender: UIBarButtonItem) {
        self.unitInformationTextFields.forEach{
            $0.resignFirstResponder()
        }
        self.validateUnitData()
        //Unit view model instance for datamodel v and v2
//        let unit = Unit(unitId: self.unitViewModel.unitId,
//                        landlord: Landlord(name: self.unitViewModel.landlordName),
//                        tenant: Tenant(name: self.unitViewModel.tenantName),
//                        notes: self.unitViewModel.notes)
        
        
        //Unit view model instance for datamodel v3
        let unit = Unit(unitId: self.unitViewModel.unitId,
                        rent: self.unitViewModel.rent,
                        landlord: Landlord(name: self.unitViewModel.landlordName),
                        tenant: Tenant(name: self.unitViewModel.tenantName),
                        notes: self.unitViewModel.notes)
        
        //Unit view model instance for datamodel v4
//        let unit = Unit(unitId: self.unitViewModel.unitId,
//                        lease: Lease(rent: self.unitViewModel.rent),
//                        landlord: Landlord(name: self.unitViewModel.landlordName),
//                        tenant: Tenant(name: self.unitViewModel.tenantName),
//                        notes: self.unitViewModel.notes)
        
        self.coreDataController.createNewUnit(unit: unit)
        self.delegate?.added(newUnit: self.unitViewModel)
        self.navigationController?.popViewController(animated: false)
    }
    
    private func updateUnitViewModel(textField: TextFields, text: String) {
        switch textField {
        case .unitNumber:
            self.unitViewModel.unitId = text
        case .landlordName:
            self.unitViewModel.landlordName = text
        case .tenantName:
            self.unitViewModel.tenantName = text
        case .rent:
            self.unitViewModel.rent = Int(text)
        case .notes:
            self.unitViewModel.notes = text
        }
    }
    
    private func validateUnitData() {
        if (self.unitViewModel.unitId?.isEmpty ?? false || self.unitViewModel.unitId == nil) {
            self.presentAlertViewController(title: "Unit Id empty", message: "Enter unit number to save.")
            return
        }
    }
}

extension AddUnitViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, let _textField = TextFields(rawValue: textField.tag), !(text.isEmpty) {
            self.updateUnitViewModel(textField: _textField, text: text)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddUnitViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textField: UITextView) {
        if let _text = textField.text {
            self.updateUnitViewModel(textField: TextFields.notes, text: _text)
        }
    }
}
