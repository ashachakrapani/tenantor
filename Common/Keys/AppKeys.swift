//
//  AppKeys.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 10/26/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

struct AppFilePathKeys {
    static let appDocumentPath = URL.documentsURL
}
