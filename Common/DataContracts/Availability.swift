//
//  Availability.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/3/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation

enum Availability: Int {
    case availableForLease = 1
    case leased
}
