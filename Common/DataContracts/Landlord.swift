//
//  Landlord.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation

struct Landlord {
    
    //This struct denotes all informaiton about the Landlord such as the their name, contact information etc.
    //For the sake of simplicity, the number of attributes have been limited
    
    ///Gets or sets the landlords name
    var name: String?
    
}
