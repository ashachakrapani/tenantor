//
//  Tenant.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation

struct Tenant {
    
    //This struct denotes all informaiton about the Tenant such as the their name, contact information, occupancy information etc.
    //For the sake of simplicity, the number of attributes have been limited
    
    ///Gets or sets the tenant name
    var name: String?
    
}
