//
//  Unit.swift
//  Tenantor
//
//  Created by Asha Chakrapani on 8/2/19.
//  Copyright © 2019 Asha Chakrapani. All rights reserved.
//

import Foundation

struct Unit {
    
    //This struct denotes all informaiton about the unit such as the unit Id, floor, sq foot, related notes etc.
    //For the sake of simplicity, the number of attributes have been limited
    
    ///Gets or sets the unit id
    var unitId: String?
    
    //This is the lease information for a unit, which will eventually be moved to a difference data model object
    ///Gets or sets the rent for the unit
    var rent: Int?
    
//    var lease: Lease?
    
    ///Gets or sets the landlord's name
    var landlord: Landlord?
    
    ///Gets or sets the tenant's name
    var tenant: Tenant?
    
    ///Gets or sets notes associated with the unit
    var notes: String?
    
}
