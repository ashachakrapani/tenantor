//
//  URL+Extension.swift
//  ProgressiveMigration
//
//  Created by Asha Chakrapani on 10/26/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

extension URL {
    static var documentsURL: URL {
        get {
            return URL(fileURLWithPath:NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        }
    }
    
    static var cachesURL: URL {
        get {
            return URL(fileURLWithPath:NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0])
        }
    }
    
    ///Updates the file URLs relative path to the documents path component.
    static func updateDocumentsRelative(url: URL) -> URL {
        if (url.isFileURL) {
            let components = url.pathComponents
            if let index = components.firstIndex( where: { $0 == "Documents" }) {
                let dicedComponents = components[(index + 1)...]
                let pathString = Array(dicedComponents).joined(separator: "/")
                let newURL = URL.init(string: pathString, relativeTo: URL.documentsURL)
                return newURL ?? url
            }
        }
        return url
    }
}
