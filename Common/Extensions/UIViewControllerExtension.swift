//
//  ViewController.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/14/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func presentAlertViewController(title: String?, message: String?, actions: [UIAlertAction] = []) {
        
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)

        if (actions.count > 0) {
            for action in actions {
                alertViewController.addAction(action)
            }
        } else {
            let okTitleLocalized = NSLocalizedString("OK", comment: "Title for Alert action.")
            let defaultAction = UIAlertAction(title: okTitleLocalized, style: UIAlertAction.Style.default, handler: nil)
            alertViewController.addAction(defaultAction)
        }
        
        self.present(alertViewController, animated: true, completion: nil)
    }

}
